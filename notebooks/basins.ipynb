{
 "cells": [
  {
   "cell_type": "markdown",
   "source": [
    "# Delineation of (sub)basins"
   ],
   "metadata": {}
  },
  {
   "cell_type": "markdown",
   "source": [
    "Here we assume that flow directions are known. We read the flow direction raster data, including meta-data, using [rasterio](https://rasterio.readthedocs.io/en/latest/) and parse it to a pyflwdir `FlwDirRaster` object, see earlier examples for more background."
   ],
   "metadata": {}
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "source": [
    "# import pyflwdir, some dependencies and convenience methods\n",
    "import geopandas as gpd\n",
    "import numpy as np\n",
    "import rasterio\n",
    "from utils import vectorize  # convenience method to vectorize rasters\n",
    "from utils import quickplot, colors, cm  # data specific quick plot method\n",
    "import pyflwdir\n",
    "\n",
    "# read and parse data\n",
    "with rasterio.open(\"rhine_d8.tif\", \"r\") as src:\n",
    "    flwdir = src.read(1)\n",
    "    crs = src.crs\n",
    "    flw = pyflwdir.from_array(\n",
    "        flwdir,\n",
    "        ftype=\"d8\",\n",
    "        transform=src.transform,\n",
    "        latlon=crs.is_geographic,\n",
    "        cache=True,\n",
    "    )"
   ],
   "outputs": [],
   "metadata": {}
  },
  {
   "cell_type": "markdown",
   "source": [
    "## Outlet based (sub)basins"
   ],
   "metadata": {}
  },
  {
   "cell_type": "markdown",
   "source": [
    "The [basins()](reference.rst#pyflwdir.FlwdirRaster.basins) method delineates (sub)basins defined by its outlet location. \r\n",
    "By default the method uses pits from the flow direction raster as outlets to delineate basins, but if outlet locations are profided these are used instead. An additional `streams` argument can be added to make sure the outlet locations are snapped to the nearest downstream stream cell, using the [snap()](reference.rst#pyflwdir.FlwdirRaster.snap) method under the hood. Here streams are defined by a minimum Strahler stream order of 4."
   ],
   "metadata": {}
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "source": [
    "# define output locations\n",
    "x, y = np.array([4.67916667, 7.60416667]), np.array([51.72083333, 50.3625])\n",
    "gdf_out = gpd.GeoSeries(gpd.points_from_xy(x, y, crs=4326))\n",
    "# delineate subbasins\n",
    "subbasins = flw.basins(xy=(x, y), streams=flw.stream_order() >= 4)\n",
    "# vectorize subbasins using the vectorize convenience method from utils.py\n",
    "gdf_bas = vectorize(subbasins.astype(np.int32), 0, flw.transform, name=\"basin\")\n",
    "gdf_bas.head()"
   ],
   "outputs": [],
   "metadata": {}
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "source": [
    "# plot\n",
    "# key-word arguments passed to GeoDataFrame.plot()\n",
    "gpd_plot_kwds = dict(\n",
    "    column=\"basin\",\n",
    "    cmap=cm.Set3,\n",
    "    legend=True,\n",
    "    categorical=True,\n",
    "    legend_kwds=dict(title=\"Basin ID [-]\"),\n",
    "    alpha=0.5,\n",
    "    edgecolor=\"black\",\n",
    "    linewidth=0.8,\n",
    ")\n",
    "points = (gdf_out, dict(color=\"red\", markersize=20))\n",
    "bas = (gdf_bas, gpd_plot_kwds)\n",
    "# plot using quickplot convenience method from utils.py\n",
    "ax = quickplot([bas, points], title=\"Basins from point outlets\", filename=\"flw_basins\")"
   ],
   "outputs": [],
   "metadata": {}
  },
  {
   "cell_type": "markdown",
   "source": [
    "## Stream order subbasins"
   ],
   "metadata": {}
  },
  {
   "cell_type": "markdown",
   "source": [
    "The [subbasins_streamorder()](reference.rst#pyflwdir.FlwdirRaster.subbasins_streamorder) method creates subbasins at all confluences where each branch has a minimal stream order set by `min_sto`. An optional mask can be added to select a subset of the outlets (i.e. confluences) which are located inside the mask."
   ],
   "metadata": {}
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "source": [
    "# calculate subbasins with a minimum stream order 7 and its outlets\n",
    "subbas, idxs_out = flw.subbasins_streamorder(min_sto=7, mask=None)\n",
    "# transfrom map and point locations to GeoDataFrames\n",
    "gdf_subbas = vectorize(subbas.astype(np.int32), 0, flw.transform, name=\"basin\")\n",
    "gdf_out = gpd.GeoSeries(gpd.points_from_xy(*flw.xy(idxs_out), crs=4326))\n",
    "# plot\n",
    "gpd_plot_kwds = dict(\n",
    "    column=\"basin\", cmap=cm.Set3, edgecolor=\"black\", alpha=0.6, linewidth=0.5\n",
    ")\n",
    "bas = (gdf_subbas, gpd_plot_kwds)\n",
    "points = (gdf_out, dict(color=\"k\", markersize=20))\n",
    "title = \"Subbasins based on a minimum stream order\"\n",
    "ax = quickplot([bas, points], title=title, filename=\"flw_subbasins\")"
   ],
   "outputs": [],
   "metadata": {}
  },
  {
   "cell_type": "markdown",
   "source": [
    "## Pfafstetter subbasins"
   ],
   "metadata": {}
  },
  {
   "cell_type": "markdown",
   "source": [
    "The [subbasins_pfafstetter()](reference.rst#pyflwdir.FlwdirRaster.subbasins_pfafstetter) method creates subbasins with the hierarchical pfafstetter coding system. It is designed such that topological information is embedded in the code, which makes it easy to determine whether a subbasin is downstream of another subbasin. At each level the four largest subbasins have even numbers and five largest interbasins have odd numbers. The `depth` argument is used to set the number of subbasin levels, i.e.: `depth=1` nine, and with `depth=2` 81 sub/interbasins are found. The `subbasins_pfafstetter` method requires upstream area of each cell, which is calculated on the fly if not provided with the `uparea` argument."
   ],
   "metadata": {}
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "source": [
    "# get the first level nine pfafstetter basins\n",
    "pfafbas1, idxs_out = flw.subbasins_pfafstetter(depth=1)\n",
    "# vectorize raster to obtain polygons\n",
    "gdf_pfaf1 = vectorize(pfafbas1.astype(np.int32), 0, flw.transform, name=\"pfaf\")\n",
    "gdf_out = gpd.GeoSeries(gpd.points_from_xy(*flw.xy(idxs_out), crs=4326))\n",
    "gdf_pfaf1.head()"
   ],
   "outputs": [],
   "metadata": {}
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "source": [
    "# plot\n",
    "gpd_plot_kwds = dict(\n",
    "    column=\"pfaf\",\n",
    "    cmap=cm.Set3_r,\n",
    "    legend=True,\n",
    "    categorical=True,\n",
    "    legend_kwds=dict(title=\"Pfafstetter \\nlevel 1 index [-]\", ncol=3),\n",
    "    alpha=0.6,\n",
    "    edgecolor=\"black\",\n",
    "    linewidth=0.4,\n",
    ")\n",
    "\n",
    "points = (gdf_out, dict(color=\"k\", markersize=20))\n",
    "bas = (gdf_pfaf1, gpd_plot_kwds)\n",
    "title = \"Subbasins based on pfafstetter coding (level=1)\"\n",
    "ax = quickplot([bas, points], title=title, filename=\"flw_pfafbas1\")"
   ],
   "outputs": [],
   "metadata": {}
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "source": [
    "# lets create a second pfafstetter layer with a minimum subbasin area of 5000 km2\n",
    "pfafbas2, idxs_out = flw.subbasins_pfafstetter(depth=2, upa_min=5000)\n",
    "gdf_pfaf2 = vectorize(pfafbas2.astype(np.int32), 0, flw.transform, name=\"pfaf2\")\n",
    "gdf_out = gpd.GeoSeries(gpd.points_from_xy(*flw.xy(idxs_out), crs=4326))\n",
    "gdf_pfaf2[\"pfaf\"] = gdf_pfaf2[\"pfaf2\"] // 10\n",
    "gdf_pfaf2.head()"
   ],
   "outputs": [],
   "metadata": {}
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "source": [
    "# plot\n",
    "bas = (gdf_pfaf2, gpd_plot_kwds)\n",
    "points = (gdf_out, dict(color=\"k\", markersize=20))\n",
    "title = \"Subbasins based on pfafstetter coding (level=2)\"\n",
    "ax = quickplot([bas, points], title=title, filename=\"flw_pfafbas2\")"
   ],
   "outputs": [],
   "metadata": {}
  },
  {
   "cell_type": "markdown",
   "source": [
    "## Minimal area based subbasins"
   ],
   "metadata": {}
  },
  {
   "cell_type": "markdown",
   "source": [
    "The [subbasins_area()](reference.rst#pyflwdir.FlwdirRaster.subbasins_area) method creates subbasins with a minimal area of `area_min`.\r\n",
    "Moving upstream from the basin outlets a new subbasin starts at tributaries with a contributing area larger than `area_min` and new interbasins when its area exceeds the `area_min`."
   ],
   "metadata": {}
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "source": [
    "# calculate subbasins with a minimum stream order 7 and its outlets\n",
    "min_area = 2000\n",
    "subbas, idxs_out = flw.subbasins_area(min_area)\n",
    "# transfrom map and point locations to GeoDataFrames\n",
    "gdf_subbas = vectorize(subbas.astype(np.int32), 0, flw.transform, name=\"basin\")\n",
    "# randomize index for visualization\n",
    "basids = gdf_subbas[\"basin\"].values\n",
    "gdf_subbas[\"color\"] = np.random.choice(basids, size=basids.size, replace=False)\n",
    "# plot\n",
    "gpd_plot_kwds = dict(\n",
    "    column=\"color\", cmap=cm.Set3, edgecolor=\"black\", alpha=0.6, linewidth=0.5\n",
    ")\n",
    "bas = (gdf_subbas, gpd_plot_kwds)\n",
    "title = f\"Subbasins based on a minimum area of {min_area} km2\"\n",
    "ax = quickplot([bas], title=title, filename=\"flw_subbasins_area\")"
   ],
   "outputs": [],
   "metadata": {}
  }
 ],
 "metadata": {
  "language_info": {
   "name": "python",
   "version": "3.9.7",
   "mimetype": "text/x-python",
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "pygments_lexer": "ipython3",
   "nbconvert_exporter": "python",
   "file_extension": ".py"
  },
  "kernelspec": {
   "name": "python3",
   "display_name": "Python 3.9.7 64-bit ('pyflwdir': conda)"
  },
  "interpreter": {
   "hash": "1d6565df54f631b3318aa7a4d6d8532e5ca49bf98056c57553f6cb4464998323"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}